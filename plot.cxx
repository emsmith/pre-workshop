// stdlib functionality
#include <iostream>
#include <vector>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <TROOT.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"


int main() {

TH1F* mjj      =new TH1F("mjj","",100,0,500);
TH1F* jet_n    =new TH1F("jet number","",20,0,20);


  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    //std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

	int jetcounter = 0;
	
	std::vector <TLorentzVector> jetlist;
	
    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
    
      TLorentzVector dijet;
      dijet.SetPtEtaPhiM (jet->pt(), jet->eta(), jet->phi(), jet->m());
      
      // print the kinematics of each jet in the event
      std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      jetcounter ++;
      jetlist.push_back (dijet);
      
    }
    
    std::cout <<"dijet mass " << jetlist[0].M() << "; " <<jetlist[1].M() << "; "<< (jetlist[0] + jetlist[1]).M() <<std::endl;
    mjj ->Fill( (jetlist[0] + jetlist[1]).M() /1000  );
    jet_n->Fill(jetcounter);
   
    // counter for the number of events analyzed thus far
    count += 1;
  }

	TFile *f = new TFile ("plots.root", "recreate");
	mjj->Write();
	jet_n->Write();
	f ->Close();

  // exit from the main function cleanly
  return 0;
}